//
//  main.cpp
//  permutation
//
//  Created by Vivek Poonia on 06/08/15.
//  Copyright (c) 2015 Vivek Poonia. All rights reserved.
//  Program creates permutations of numbers in the vector.

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main(int argc, const char * argv[]) {
    
    vector<int> v;
    for (int i=1; i <=5; i++) {
        v.push_back(i);//inserts element into the vector
    }
    
    do {

        //initializes the iterator
        typeof(v.begin()) it = v.begin();
        //prints out the vector
        for(; it != v.end();it++){
            cout<< *it;
        }
        cout<<endl;
    } while (std::next_permutation(v.begin(), v.end()));
    //computes one permutation of set of numbers on each iteration.
    
    return 0;
    
}
